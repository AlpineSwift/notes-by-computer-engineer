# Scribbles on Computer Science Topics

# Intent
Basic, human readable, explanations on basic concepts found in Computer Science and related fields.

# Why This?
Though there are dozens of resources available on the topic, this project is an attempt to explain some of those concepts using simpler text, and helpful sketches.

# Live
View all published articles on [gitlab.io](https://alpineswift.gitlab.io/notes-by-computer-engineer/)
