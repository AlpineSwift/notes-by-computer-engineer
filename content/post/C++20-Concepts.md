+++
author = "Alpine Swift"
title = "C++-20 Concepts: Enforcing Feature Requirements Natively Using C++ Language Feature(s)."
date = "2021-12-28"
description = "C++20 language feature: concepts, and why it is useful. And, how to achieve something similar in C++17."
tags = [
    "C++",
    "Programming",
    "C++20 Concepts",
]
+++
Here is a brief summary of the C++-20 language feature: `concept`s which can be used to enforce and possibly track function requirements natively C++ language feature.
A brief example of how to achieve a similar objective using C++-17, e.g., via `enable_if`.
<!--more-->

# What Problem Do We Solve?
We use C++-20 language feature `concept` to enforce a requirement e.g., _a CircularBuffer of 0 capacity shall not exist._ The `concept` provides a way to create a _named_ requirement which must always be satisfied by a class or a method.

Also described further in this article is to achieve a similar result without using `concept` feature of C++-20.

## Full Project
All source code along with unit tests are available [here](https://github.com/RomanoViolet/C-/tree/master/CircularBuffer).


## Running Example: A Circular Buffer

We have a CircularBuffer class declared like so:

```c++ {linenos=true}
// A CircularBuffer class which takes the type information of the elements to be held,
// and the capacity, C of the CircularBuffer.
template < typename T, uint8_t C >
class CircularBuffer
{
  // stuff here.
};
```

### The Requirement: CircularBuffer Shall Not Be Zero capacity
A functional requirement posed upon the `CircularBuffer` class is: _A zero capacity CircularBuffer object shall not be created_.


## Approach 1: C++-20 `concept`s:

The requirement states that a CircularBuffer of zero capacity cannot be instantiated.
Formulate the requirement using `concept` like so:

```c++ {linenos=true}
template < typename T, uint8_t C >
//       └------------------------┘
//                   |
//   the concept takes the same template parameters
//   as the CircularBuffer class to which it will be applied.
concept NonZeroCapacityOfBuffer = C > 0U; //NOLINT
// |              |               └-----┘
// |              |                  |
// keyword    name of requirement    |___ the actual requirement
//            (technically, concept)      to be enforced.
```

### Apply The Concept To The `CircularBuffer` Class
Associate the `CircularBuffer` class with the requirement (i.e., `concept NonZeroCapacityOfBuffer`):

```c++ {linenos=true}  
    template < typename T, uint8_t C >
//     ----- Binds the class CircularBuffer with the requirement,
//     |     i.e., concept NonZeroCapacityOfBuffer
//     |
//  ┌------┐
    requires NonZeroCapacityOfBuffer< T, C >
// └----------------------------------------┘
//     |
//     ---- Class CircularBuffer now implements the requirement that
//          a zero-capacity CircularBuffer cannot be instantiated.
    class CircularBuffer
    {};
```
### Test
The following (google) unit test will fail as expected:

```c++ {linenos=true}
TEST ( ShouldNotCompile, CircularBufferWithZeroCapacity )  // NOLINT
{
    CircularBuffer<int, 0> b{};
}
```

Here's what `gcc-11.2` (x86-64) says:
```bash {linenos=true}
<source>: In member function 'virtual void ShouldNotCompile_CircularBufferWithZeroCapacity_Test::TestBody()':
<source>:31:20: error: template constraint failure for 'template<class T, unsigned char C>  requires  NonZeroCapacityOfBuffer<T, C> class CircularBuffer'
   31 |         CircularBuffer<int, 0> b{};
      |                    ^
<source>:31:20: note: constraints not satisfied
<source>: In substitution of 'template<class T, unsigned char C>  requires  NonZeroCapacityOfBuffer<T, C> class CircularBuffer [with T = int; unsigned char C = 0]':
<source>:31:20:   required from here
<source>:10:9:   required for the satisfaction of 'NonZeroCapacityOfBuffer<T, C>' [with T = int; C = 0]
<source>:10:37: note: the expression 'C > 0 [with C = 0]' evaluated to 'false'
   10 | concept NonZeroCapacityOfBuffer = C > 0U; //NOLINT
      |                                   ~~^~~~
Compiler returned: 1
```

### Run Negative And Positive Unit Tests
The use-case described here calls for unit tests which _are expected_ to lead to a compile-time error. Generally, this will break the unit-test stage if running in the continuous integration pipeline. One method to get unit tests which cause the compiler-error are described [here](https://alpineswift.gitlab.io/notes-by-computer-engineer/post/c++-compilationsuccessunittests/).

## Approach 2: Using `enable_if`

### An Intentionally Incompletely Defined Class
```c++ {linenos=true}
template < typename T, uint8_t C, typename = void >
class CircularBuffer; // <-- notice that the class body does not exist.
```
In addition to the expected `typename T, uint8_t C` parameters, an extra unnamed template parameter `typename = void` is added at the end. Therefore, instantiation of  `CircularBuffer` in which the last parameter evaluates to `void` will cause the compiler to abort - a mechanism used further down to enforce a requirement.

### Add The Conditionally Defined Class `CircularBuffer`

Now define the class `CircularBuffer` in which the last template argument (matched against parameter `typename = void` declared first) evaluates the requirement (i.e., a zero-capacity circular buffer cannot be instantiated) inside an `enable_if_t`.
The `enable_if_t` produces a `void` in case the boolean evaluates to false, or type otherwise, e.g., `true_type`.

```c++ {linenos=true}
template < typename T, uint8_t C >
//                           if C > 0, "true_type", "void" otherwise
//                               |
//                          ┌-------------------------┐
class CircularBuffer< T, C, std::enable_if_t< ( C > 0 ) >>
//                                            └------┘
//                                              |
//                                              └-- Requirement to be enforced.
{};
```


### Test
The following (google) unit test will fail as expected:

```c++ {linenos=true}
TEST(ShouldNotCompile, CircularBufferWithZeroCapacity)
{
    CircularBuffer<int,0U> b{};
}
```

Attempting the compile with `gcc-11.2` x86_64 produces the following message:
```bash
<source>: In member function 'virtual void ShouldNotCompile_CircularBufferWithZeroCapacity_Test::TestBody()':
<source>:20:28: error: variable 'CircularBuffer<int, 0> b' has initializer but incomplete type
   20 |     CircularBuffer<int,0U> b{};
      |                            ^
Compiler returned: 1
```
