+++
author = "Alpine Swift"
title = "Compiler, Compiler, Does It Compile? This Is The Test!"
date = "2021-08-09"
description = "How to design and run unit tests which test whether a piece of code can be successfully compiled? Oh, and no extra frameworks."
tags = [
    "C++",
    "Programming",
    "Unit Tests"
]
+++

# What Is This About?
Normally, unit tests are built _first_, wherein a binary is generated, which is then executed.
However, there are times when the purpose of a (unit) test is to assess whether a piece of code can be successfully compiled.
Most often in this case, the unit-test is considered to have *failed* if the compiler can successfully compile the code snippet, otherwise, the unit-test is considered to have passed. There are approaches to design and run such unit-tests, often relying on additional frameworks or niche approaches.
This article explains how to design and run such unit tests for the rest-of-us, using common and near-universally used framework such as [`gtest`](https://github.com/google/googletest), [`cmake`](https://cmake.org/), and a compatible compiler only.
<!--more-->

# Reference Sources
Get the source code upon which this article is based [here](https://gitlab.com/AlpineSwift/cpp-17/-/tree/article/negative-compiler-tests).
The source code is part of a [`cmake`](https://cmake.org/) based C++ project.
Get the branch like so:
```bash
git clone https://gitlab.com/AlpineSwift/cpp-17.git --branch article/negative-compiler-tests --single-branch sandbox               
```
The `--single-branch` option tells git that we are interested in cloning only the branch `article/negative-compiler-tests` of the repository, also called *shallow cloning*.

If the cloning is successful, a folder `sandbox` will contain the reference source code used in this article.


# Context
Two types of unit tests are built in this article: 
1. Executable-independent unit tests
2. Unit-test executable dependent unit tests

Executable-independent unit tests do not require the usual *build* step of the software project, since no test-binary is required. 
In this example, running the unit test requires invoking the compiler on a piece of logic (the unit-test, see example below) and assessing
whether the compiler exits with an error (i.e., a non-zero exit code). The unit test is considered to have been *passed* if the compiler
returns with a non-zero exit code (and therefore, a unit-test executable cannot be generated).

Unit-test executable dependent unit test requre the usual *build* step to be completed first. 
Specifically, a piece of logic is passed onto the compiler, and it is expected that the compiler will successfully compile (and later, the linker will link) the unit test into an executable. Running the unit test requires invoking this newly generated executable and assessing the results of expressions contained in it. 

# Approach
We will use `CTest` unit test engine of `CMake` in order to run both types of tests. 
`CTest` requires specifying the executable that must be called in order to run the unit-tests.
Since no executable is expected to be produced for Executable-independent unit tests, we will provide a small shell-script
which can be called by `CTest` instead.

For Unit-test executable dependent unit tests, the name of executable generated in the build step is provided.
```bash
                                       Compiler + Linker
                                                |
                                                |
Unit-test executable dependent unit test(.cpp) -+-(generates)-> binary -(called by CTest)-> CTest reads unit test results

                                                                            Compiler
                                                                               |
CTest -(calls) -> custom shell script -(generates)-> Temporary CMakeLists.txt -+-> (invokes compiler)-> CTest reads exit code: Zero or Non-Zero
                                                                               |
                                                              Executable-independent unit tests (.cpp)

```

# Setup
Let's declare a class `GeneralClass` like so (`negative_compile_tests/header.hpp` in the sources):

```c++
#ifndef HEADER_HPP_INCLUDED
#define HEADER_HPP_INCLUDED
#include <type_traits>
template < typename T > class GeneralClass
{
   public:
    explicit GeneralClass ( int a ) : a_ ( a )
    {
        // do not allow instantiation for T = float
        static_assert ( ! std::is_floating_point< T >::value,
                        "GeneralClass must not be instantiated for floats." );
    };

    auto getValue ( ) const -> int { return this->a_; }

   protected:
   private:
    int a_;
};
```

This class can be instantiated successfully for all `T` except for `float`. 

We would like to write a gtest based unit test which will verify that it is
indeed not possible to instantiate an object of class `GeneralClass` with `T=float`.
Here is the unit test, written inside a google-test framework (see `negative_compile_tests/negative_unit_tests.cpp` in sources):

```c++
#include <gtest/gtest.h>

#include "header.hpp"

TEST ( ShouldNotCompile, Test1 )  // NOLINT
{
    GeneralClass< float > d ( 10 );
    EXPECT_TRUE ( true );
}

```
The compiler will not be able to successfully compile this unit-test due to the triggering of the `static_assert` statement in the class `GeneralClass`.


The `CMakeLists` is then:

{{< highlight cmake "linenos=table,hl_lines=8 15-17,linenostart=5" >}}

enable_testing() # Tells CMake that we would like to build unit tests too.

add_test(
    # the name of the unit test
    NAME NEGATIVE_COMPILER_TESTS 
    #
    # The location where the test lives
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    #
    # the command to be executed in order to execute the unit test.
    # Either the command refers to an executable built by cmake, which
    # is the usual case, or refers to a pre-existing executable or a script
    # which can be called by cmake's testing engine (ctest).
    # In this example, we provide the script to be executed by ctest.
    COMMAND ${CMAKE_CURRENT_LIST_DIR}/runNegativeCompilerTests.sh 
    )

# Tell the testing engine of cmake that we are going to specify
# extra properties for the test.
set_tests_properties(
    #
    # the name of the unit test for which the properties are specified.
    NEGATIVE_COMPILER_TESTS
    #
    # Tell ctest that calling the script (here, runNegativeCompilerTests.sh)
    # will cause the process to exit with a non-zero error code. Specifically, ctest
    # must expect a non-zero error code as a proof of that the unit test has passed.
    PROPERTIES WILL_FAIL
    #
    # When the process fails, it will generate an error message. 
    # Tell ctest that we are looking for "static_assert" string in the 
    # error message. Any other string will cause the unit test to _fail_.
    PROPERTIES PASS_REGULAR_EXPRESSION "static_assert")

{{< / highlight >}}

In the reference example, feel free to comment out all other lines from `negative_compile_tests/CMakeLists.txt`. The purpose of extra lines will be described further down in this article.

# The Custom Test Wrapper, `runNegativeCompilerTests.sh`
This shell script is called by cmake's test engine, `ctest`. 
The purpose of this script is to generate a temporary `CMakeLists.txt` which will be
use in the later steps of this script to invoke the compiler with the unit test described above.
The compiler is then expected to abort (i.e., exit with a non-zero error code).
The unit test is considered to have passed if the compiler aborts.
Here's the shell script:

{{< highlight bash "linenostart=1" >}}

#!/usr/bin/env bash
# This script will run from ./sandbox/build/negative-compiler-tests folder.
# IMPORTANT: Make sure to apply corrections for the path on your machine.
#            
# Remove any previous "negative-compiler-tests" folder
project_root=$(pwd)
if [[ -d "${project_root}/../build/negative-compiler-tests" ]]
then
    rm -rf ${project_root}/../build/negative-compiler-tests
fi

# Create a location "negative-compiler-tests" inside the build parent folder
mkdir ${project_root}/../build/negative-compiler-tests

# Copy the sources (.cpp, .hpp) necessary for running the 
# NEGATIVE_COMPILER_TESTS unit test.
cp -r ./negative_unit_tests.cpp ./*.hpp ${project_root}/../build/negative-compiler-tests/

# Remove the CMakeLists we wrote above, which triggers this shell script.
# The temporary CMakeLists.txt we create will be generated from this script.
rm -rf ${project_root}/../build/negative-compiler-tests/CMakeLists.txt
touch ${project_root}/../build/negative-compiler-tests/CMakeLists.txt

# The contents of the minimal temporary CMakeLists.txt
# required for compiling the negative_unit_tests.cpp containing the
# unit test. 
echo 'cmake_minimum_required(VERSION 3.16)' >> ${project_root}/../build/negative-compiler-tests/CMakeLists.txt
echo 'project(demo LANGUAGES CXX)' >> ${project_root}/../build/negative-compiler-tests/CMakeLists.txt
echo "find_package(GTest REQUIRED)" >> ${project_root}/../build/negative-compiler-tests/CMakeLists.txt
echo 'set(CMAKE_CXX_STANDARD 17)'>> ${project_root}/../build/negative-compiler-tests/CMakeLists.txt
echo 'add_executable(RUN_NEGATIVE_TESTS ${CMAKE_CURRENT_LIST_DIR}/negative_unit_tests.cpp)' >> ${project_root}/../build/negative-compiler-tests/CMakeLists.txt

# Move to the newly created folder
cd ${project_root}/../build/negative-compiler-tests
#
# Create build folder for out-of-tree build
mkdir ./build
cd ./build
#
# Run cmake
cmake .. 
#
# Now build
make all
#
# If the build throws an error containing the string 
# "static_assert",  we echo a simple string "static_assert"
# The reason is to avoid specifying complex multi-line regular expressions
# for PROPERTIES PASS_REGULAR_EXPRESSION for the API set_tests_properties
# above.
if grep -F "static_assert" result.log
then
    echo "static_assert"
fi

{{< / highlight >}}

# Run The Test!
Inside the `sandbox` folder, open up a terminal and run:

1. Create a build directory where all temporary artifacts and any binaries will be located:

```bash
mkdir build
```

2. Proceed inside the `build` folder:
```bash
cd build
```

3. Run `cmake`:
```bash
cmake ..
```

4. Build
```bash
make all
```

5. Run the unit test we created:
```bash
make test
```

Here's the expected output:
```bash
Running tests...
Test project /workspaces/cpp-17/build
    Start 1: NEGATIVE_COMPILER_TESTS
1/1 Test #1: NEGATIVE_COMPILER_TESTS ..........   Passed    1.46 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   1.46 sec
```

## Observations
The `make all` actually does not build any executable - in the example above, it does nothing, since there is no executable to build.
Remember, the unit-test examines the exit code returned by the compiler when the compiler is called on to compile `negative_compiler_tests/negative_unit_tests.cpp`.
Also, the unit test *expects* the compiler to exit with an error (i.e., return a non-zero error code).

# Run It Together With Unit-test Executable Dependent Unit Tests
Unit-test executable dependent unit tests are first built to get an executable, which is then executed by a unit-test framework, such as `ctest`. 
A software unit (e.g., a class, or a project itself) may feature both kinds of unit-tests: the type that do not require an executible to be built first, as in the example above, and the second type: which require an executable to be successfully built first.

## The Second Unit Test
Here is a new unit test which must be first compiled to an executable and then run by the test framework, `ctest` (`negative_compiler_tests/usual_unit_tests.cpp` in sources). 

{{< highlight cpp "linenostart=1" >}}

#include <gtest/gtest.h>

#include "header.hpp"

TEST ( TESTSUITE, ShouldBeBuiltFirst )  // NOLINT
{
    GeneralClass< int > u { 20U };
    EXPECT_EQ ( 20U, u.getValue ( ) );
}


{{< / highlight >}}

The `CMakeLists.txt` is then updated as follows with the lines added to the original `CMakeLists.txt` are preceeded by a comment line
beginning with "# New".

{{< highlight cmake "linenostart=1" >}}

# New: Tell cmake to find the gtest package
find_package(GTest REQUIRED)

enable_testing()

# New: Build the executable with the name "UnitTestExecutable"
#      from the source usual_unit_tests.cpp
add_executable(UnitTestExecutable
               ${CMAKE_CURRENT_LIST_DIR}/usual_unit_tests.cpp)

# New: Automatically adds all tests which are compiled into the executable
#      (here, UnitTestExecutable) to be run by CTest.
#      This will help add unit tests written in e.g., usual_unit_tests.cpp
#      be automatically included into the executable and run by CTest
gtest_discover_tests(UnitTestExecutable)

# New:  Tell CMake that the sources are C++, and should be linked as such.
set_target_properties(UnitTestExecutable PROPERTIES LINKER_LANGUAGE CXX)

add_test(
    NAME NEGATIVE_COMPILER_TESTS
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    COMMAND ${CMAKE_CURRENT_LIST_DIR}/runNegativeCompilerTests.sh)

set_tests_properties(
    NEGATIVE_COMPILER_TESTS
    PROPERTIES WILL_FAIL
    PROPERTIES PASS_REGULAR_EXPRESSION "static_assert")

# New: Link the executable to necessary gtest libraries
target_link_libraries(
    UnitTestExecutable
    ${GTEST_LIBRARY}
    ${GTEST_MAIN_LIBRARY}
    pthread
    gtest_main
    )

{{< / highlight >}}

# Run All The Tests!
As before, inside the `sandbox` folder, open up a terminal and run:
```bash
cmake ..
make all
make test
```

This time, `make all` builds the executable from the sources in `negative_compiler_tests/usual_unit_tests.cpp`:
```bash
Scanning dependencies of target UnitTestExecutable
[ 50%] Building CXX object negative_compiler_tests/CMakeFiles/UnitTestExecutable.dir/usual_unit_tests.cpp.o
[100%] Linking CXX executable UnitTestExecutable
[100%] Built target UnitTestExecutable
```

The `make test` command will call `CTest` engine to execute both types of unit tests:

```bash
Running tests...
Test project /workspaces/cpp-17/build
    Start 1: TESTSUITE.ShouldBeBuiltFirst_1
1/2 Test #1: TESTSUITE.ShouldBeBuiltFirst_1 ...   Passed    0.00 sec
    Start 2: NEGATIVE_COMPILER_TESTS
2/2 Test #2: NEGATIVE_COMPILER_TESTS ..........   Passed    2.22 sec

100% tests passed, 0 tests failed out of 2

Total Test time (real) =   2.23 sec
```
