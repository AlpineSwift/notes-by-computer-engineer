+++
author = "Alpine Swift"
title = "Bayes' Theorem"
date = "2021-03-28"
description = "What the magic is?"
tags = [
    "Bayes Theorem",
    "Probability",
]
+++
We answer everyday questions like:
| Question   | |     Tool      |
|:----------|:---:|-------------:|
| What is the chance of rain when it is cloudy today? based on weather data for
 the last years which provides information about the number of rainy days in the
  last years, number of cloudy days both with rain, and cloudy days without rain.|  |**Conditional Probability** |
||||
||||
| What is the chance of an event happening, accounting for all possible ways it
can happen? |  |  **Total Probability**   |
||||
||||
| I want to have another look, this observation does not add up. What are the
chances that it really happened? When you are confronted with an observation
(or a test result, or data) which is stated to be highly accurate, but not 100%
accurate, so you wonder how much should you trust the observation - did you fall
in the small error margin that the observation has? |  | **Bayes’ Theorem** |
||||
||||
|You make an observation - which suggests an event (e.g., you watch a per-
son walking on the sidewalk, and suspect that he may step on the crosswalk)
However, a single observation is not enough to conclude that the event will
happen (with high confidence), so you decide to make multiple observations
to confidently decide whether or not the event may occur.||**Bayes’ Theorem** |
||||
||||
||||
Read more [here.](post/HTML/BayesTheorem_Probability.html)
<!--more-->
---
It's quite simple
