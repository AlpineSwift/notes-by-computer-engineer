+++
author = "Alpine Swift"
title = "C++ Type Traits: Oh T, Reveal Your Secrets"
date = "2021-04-27"
description = "We find out which concrete type has been instatiated from templated type, and peek at class properties."
tags = [
    "C++",
    "Programming",
]
+++

# What Are Type Traits?
Type traits provide useful information about a type (or equivalently, a class) which is available at the time of compilation. Therefore, this includes hard coded information such as the maximum integer supported by `uint8_t`, or information which becomes available as generic classes (i.e., templates) are instantiated (i.e., the template parameter is filled). With such information, it is possible to compute results of some parts of the program at compile time, which results in lower run-times of the applications. To clarify:
```c++
//        type for which information "trait" can be queried
//           |
//         ┌---┐
std::vector<int> a{1,2,3};
//               └------┘
//                  |
//              Not compile time type information
//              i.e., not a trait.
```

<!--more-->
---
## Example From C++ Standard Template Library (C++ STL)
A simple example is to ask whether a type belongs to the [integer family](https://en.cppreference.com/w/cpp/types/is_integral). We find out by accessing the traits of the type like so:

```c++
#include <type_traits>
#include <iostream>
int main()
{
    // utility: converts 0 to false
    //                   1 to true                                      the question we ask ("trait")
    //                  |                                                       |
    //           ┌ ------------┐                                        ┌ ---------------┐
    std::cout << std::boolalpha << " Is float from integer family: " << std::is_integral_v<float> << std::endl;
    //                                                                  └----------------┘ └----┘                      
    //                                                                          |             |
    //                                                                          |     the exact type for which
    //                                                                          |      we want the information
    //                                                                          |
    //                                                                   the question, "trait" we query for  
    return 0;
}
```
the program prints:
```bash
Is float from integer family: false
```
The list of all available type traits supported by the `C++` STL is available at [cppreference](https://en.cppreference.com/w/cpp/header/type_traits).

## Simple Logical Operations With Type Traits
Type traits also supports simple logical operations about types, e.g., in a class with two (or more) template parameters, we can ask whether both the parameters are the same type when the template is instantiated:
```c++
#include <type_traits>
#include <iostream>

template <typename T, typename U>
class MyClass
{
    public:
    //                                     computed at compile time
    //                                     when T, U are filled in with
    //                                     concrete types.
    //                                              |
    //                                     ┌ -----------------┐
    static constexpr bool types_not_equal = std::is_same_v<T,U>;
    //                    └-------------┘
    //                           |
    //                the new trait we introduced
};

int main()
{
    std::cout << std::boolalpha << " Are both types same? " \
    //                   question we ask, i.e., the trait  
    //          T    U            |
    //          |    |     ┌ -------------┐
    << MyClass<int,float>::types_not_equal \
    // └----------------┘
    //         |
    //  context for the question
    << std::endl;
    return 0;
}
```

The generic class requires the user to fill in the definition of `T`, the template parameter.


## Simple Application: Restrict Template Instantiation Based On Logical Conditions
There are cases wherein a generic type (or, equivalently, a class) is implemented to work only with a pre-determined subset of concrete types. It is possible to implement traits for such classes which can be then used to ensure that the end-user is not able to instantiate the (generic) class with disallowed concrete types:


```c++
#include <cstdint>
#include <iostream>

// Principle
// The compiler tries to find the variant of the generic class
// which matches most closely with the ones defined here.
// If a match is found, that class, and the properties (here, trait)
// are made available.

// -----      The Trait "is_supported" ------
// Default variant of the template
// The property "value" is set to false.
// This template is instantiated in case no other variant (i.e., partial specialization)
// of this class matches more closely to the request made by the user (see main function)
template <typename T>
struct is_supported
{
    //                        default value
    //                              |
    //                           ┌------┐
    static constexpr bool value = false;
    //                   └-----┘
    //                      |
    //                    the new trait we introduce
};

// Specialization (i.e., variant) of the generic class for cases when
// the user has requested T = std::uint8_t
// Under such circumstances, this specialization is initialized.
template<>
struct is_supported<std::uint8_t>
{
    //                  Only if T = std::uint8_t
    //                         |
    //                   ┌------------┐
    static constexpr bool value = true;
};


// ----- Using the trait "is_supported" -----
// class MyTypeRestrictedClass utilizes user-constructed type-trait "is_supported".
// The class MyTypeRestrictedClass should be instantated only if
// the trait for the concrete type i.e., is_supported<T=user-supplied-concrete-type>
// yields true.
template <typename T>
struct MyTypeRestrictedClass
{
    // the trait we enquire about after T has been filled in by user
    //                   |
    //            ┌-------------┐
    static_assert(is_supported<T>::value && "The requested concrete type is not supported");

    // Compiler will reach here only if T is filled with supported concrete type
    MyTypeRestrictedClass()
    {
        std::cout << "Supported type" << std::endl;
    }
};

int main()
{
    //                T = std::uint8_t: supported type.
    //                The object c will be successfully created.
    //                          |
    //                   ┌-------------┐
    MyTypeRestrictedClass<std::uint8_t> c;

    //        T = char: unsupported type.
    //        The object d will not be created.
    //                     |
    //                   ┌----┐
    MyTypeRestrictedClass<char> d; // compiler throws an error

    return 0;
}

```
Compiler returns:

```bash
<source>: In instantiation of 'struct MyTypeRestrictedClass<char>':
<source>:66:33:   required from here
<source>:45:36: error: static assertion failed
   45 |     static_assert(is_supported<T>::value && "The requested concrete type is not supported");
      |                                    ^~~~~
<source>:45:36: note: 'is_supported<char>::value' evaluates to false
// more lines from the output not shown.
```
Notice that although there is lot of output from the compiler, the actual identified problem is highlighted correctly.


### Aside: Easier and Cleaner Implementation
An easier implementation to achieve the same type-safety goal is:

```c++
#include <cstdint>
#include <iostream>

template <typename T>
struct MyTypeRestrictedClass;

template <>
struct MyTypeRestrictedClass<std::uint8_t>
{

};

int main()
{
    MyTypeRestrictedClass<std::uint8_t> c;
    MyTypeRestrictedClass<char> d; // will cause compiler to throw error
    return 0;
}
```

The cost for the simplicity is the very confusing compiler message shown below:
```bash
<source>: In function 'int main()':
<source>:17:33: error: aggregate 'MyTypeRestrictedClass<char> d' has incomplete type and cannot be defined
   17 |     MyTypeRestrictedClass<char> d; // will cause compiler to throw error
      |                                 ^
ASM generation compiler returned: 1
<source>: In function 'int main()':
<source>:17:33: error: aggregate 'MyTypeRestrictedClass<char> d' has incomplete type and cannot be defined
   17 |     MyTypeRestrictedClass<char> d; // will cause compiler to throw error
      |                                 ^
Execution build compiler returned: 1
```

Therefore, a developer of a generic type (or, template class) must weigh very carefully the simplicity (or lack thereof) of the approach versus the burden on the end-user, specifically, the effort required to understand the compiler message in case of errors. C++ compilers are notorious for generating long, unfathomable messages specifically in cases of errors related to generic types.


## Simple Application: Restrict Runtime Properties Of A Type

In this example, we would like to place a restriction that an end-user (i.e., the application developer) should not be to create any vector container with more than 10 elements. One solution to the problem is to create a new type `MyVector` to be made available to the end-user which is constructed like so:

```c++
#include <cstdint>
#include <type_traits>
#include <iostream>
#include <array>

//      we allow MyVector to contain any type of element, i.e. T is unrestricted.
//             |
//        ┌----------┐
template < typename T, std::uint8_t Size > class MyVector
//                     └----------------┘
//                            |
//                      We ask the developer to specify how many elements should
//                      the object hold.
{
   public:
 //   isLimited is evaluated at compile time itself.
 //   type trait information is usually has public access.
 //                |   
 //┌------------------------------┐
   static constexpr bool isLimited(std::uint8_t _s)
   //     └-------┘
   //         |
   //      tells the compiler that the function (or symbol) can be evaluated at compile-time.
   {
       if (_s > 10)
       return true;
       else
       return false;
   }

   static constexpr std::uint8_t _getSize(std::uint8_t _s)
   {
       return _s > 10 ? 10 : _s;
   }
   //                    assigned a value at compile time
   //                    calls isLimited compile-time evaluated member method.
   //                         |
   //                    ┌---------┐
   constexpr static bool is_limited{isLimited(Size)};

    explicit MyVector (  ){ };

    std::uint8_t getArraySize() const { return this->size_;}
   protected:
   private:
   //  the actual standard (C++ STL) container
   //  which is wrapped by MyVector class.
   //  calls _getSize compile-time evaluated member method.
   //              |
   //┌----------------------------┐
    std::array< T, _getSize(Size) > arr_;
    constexpr static std::uint8_t size_ = _getSize(Size);
   //                            └-----┘
   //                               |
   //                             assigned at compile time.
};

int main()
{

    MyVector<std::uint8_t, 9> b{};
    std::cout << "Size of Array: " << static_cast<unsigned>(b.getArraySize()) << std::endl;
    //
    //                                                                             Information about type ("trait") we look for
    //                                                                                  |
    //                                                                             ┌---------┐
    std::cout << "Is Limited? " << static_cast<unsigned>(MyVector<std::uint8_t, 9>::is_limited) << std::endl;

    MyVector<std::uint8_t, 99> c{};
    std::cout << "Size of Array: " << static_cast<unsigned>(c.getArraySize()) << std::endl;
    std::cout << "Is Limited? " << static_cast<unsigned>(MyVector<std::uint8_t, 99>::is_limited) << std::endl;
    return 0;
}
```

Compiler returns:
```bash
Program returned: 0
Size of Array: 9
Is Limited? 0
Size of Array: 10
Is Limited? 1
```

Of course, in any production project with such constraints, there will be a mechanism, e.g., a review, or automatic code analysis, which will ensure that the code-base does not contain any use of _unrestricted_ or vanilla `std::vector`.
