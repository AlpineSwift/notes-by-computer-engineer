+++
author = "Alpine Swift"
title = "C++ enable_if: Make That Type Vanish!"
date = "2021-05-06"
description = "We talk about C++ provides us the tools to make the type declaration go away"
tags = [
    "C++",
    "Programming",
]
+++
At times, you would like your generic class (or class with templates) not work with certain types.
This post describes one way to remove the offending type *at compile time* from template specialization
and cause the compilation to fail. Read on.
<!--more-->

# Pre-Requisites

1. **(P.1)** A construct which will output a ```void``` if the expression provided to it evaluates to false, or else a valid type. We use `enable_if`.
2. **(P.2)** Non-specialized class template declaration with one (let's say, `enable`) type parameter default initialized to `void`. 
3. **(P.3)** A partial specialization of the class template with type parameter `enable` assigned the result of a boolean condition wrapped inside `enable_if`.

# The Approach

*In the example below, the class `MyRestrictedClass` is expected to be undefined of any type `T` which is non-integral*.

Declare a template (e.g., class) *partial specialization* in which one (or more) template parameters evaluate to either `void` or a valid type (e.g., `T`) when a defined expression is evaluated as in the example below:

```c++ {linenos=true}
template <typename T>
struct MyRestrictedClass<T, std::enable_if_t<std::is_integral_v<T>>>
//                          └--------------------------------------┘
//                                              |
//                        "void" for T = non integral types, T otherwise.
{
    MyRestrictedClass()
    {
        std::cout << "Class Instantiated" << std::endl;
    }
};
```

Declare the non-specialized version of this class which will be used when the boolean expression (see example above)
evaluates to false. This non-specialized class will have no body so that the type (i.e., `MyRestrictedClass`) has no definition
when the given boolean evaluates to `false`:

```c++ {linenos=true}
template <typename T, typename enable = void>
//                    └---------------┘
//                            |
//                    this class matches when the
//                    second template parameter is "void".
//                    type name "enable" may be skipped since it is not used in the
//                    class declaration or definition. 
//                   
//                    Equivalent: template <typename T, typename = void>
//
struct MyRestrictedClass; // no class body
```

This non-specialized version must appear in the header file *before* any class specializations.

# Usage
## Successful Instantiation
Instantiate the class, providing a single template parameter (for the example presented above):

```c++ {linenos=true}
auto main() -> int
{
//                compiler automatically fills the
//                second template parameter based on the
//                default value specified in the non-specialized
//                class-template declaration (here, "void").
//                Equivalent declaration: MyRestrictedClass<int, void> b{};
//                        |
//                       ┌-┐
    MyRestrictedClass<int   > b{};
    (void)b;

    return 0;
}
```
When executed, the object `b` will be instantiated successfully:
```bash {linenos=true}
Program returned: 0
Class Instantiated
```
## Unsuccessful Instantiation
The class `MyRestrictedClass` cannot be instantiated (disappears) should the template
parameter be a non-integral type:

```c++ {linenos=true}
auto main() -> int
{
    MyRestrictedClass<double> c{};
    (void)c;

    return 0;
}
```
The compiler will not compile the snippet, as we intended:

```bash {linenos=true}
<source>:19:31: error: implicit instantiation of undefined template 'MyRestrictedClass<double>'
    MyRestrictedClass<double> c{};
                              ^
<source>:5:8: note: template is declared here
struct MyRestrictedClass;
       ^
1 error generated.
ASM generation compiler returned: 1
<source>:19:31: error: implicit instantiation of undefined template 'MyRestrictedClass<double>'
    MyRestrictedClass<double> c{};
                              ^
<source>:5:8: note: template is declared here
struct MyRestrictedClass;
       ^
1 error generated.
Execution build compiler returned: 1
```
