+++
author = "Alpine Swift"
title = "C++17: Template Type Deduction. Yay, Compiler Gets It."
date = "2021-04-17"
description = "What the magic is?"
tags = [
    "C++17",
    "Programming",
]
+++

C++17 is beginning to make more sense, syntax-wise. One improvement in the 2017 standard `C++-17` is the concept of _class template argument deduction._ In brief, if you have wondered why `C++` before 2017 standard did not feel right, or complete, you were correct. In this blog, we specifically show that a `C++17` compliant compiler is able to _infer_ the types based on arguments supplied at the time of instantiating an object. 
<!--more-->
---

## Quick Summary
In summary, with `C++17` we do not have to explicitly specify types for templated classes resulting in a shorter and cleaner code. Here is an example:

## Example 1: Less Typing In Instantiation Of Templated Classes
```c++

//        MyClass is a templated class
//             |
//        ┌ -------┐
template <typename T>
class MyClass
{
    public:
        MyClass(T arg) : _data(arg){};
    private:
    T _data;
};


int main()
{
    // Section below compiles only with C++17
    // We do not need to specify T anymore, like pre- C++17.
    // Compiler infers T = int. Will also work for multiple template arguments.
    //      |
    //     ┌-┐
    MyClass   e{6};

    // Pre C++17
    // Explicitly specify T
    //        |
    //      ┌---┐
    MyClass <int>   e{6};

    return 0;
}
```

## Example 2: Less Typing In Instantiation Of STL Classes

C++17 can also infer types while instantiating STL classes without having to explicitly specify types. An example using `std::tuple` below:


```c++

// std::tuple's interface in C++.
// Notice that the interface requires the knowledge of types at
// the time of instantiating an std::tuple.
template< class... Types >
class tuple;

#include <tuple>
int main()
{
  // Section below compiles only with C++17
  // We do not need to specify types anymore, like pre- C++17.
  // Compiler infers types: {char, double}
  //           |
  //          ┌-┐
    std::tuple   t{"a", 45.0};

  // Pre C++17
  //        Explicitly specify types
  //                  |
  //        ┌-------------------┐
  std::tuple<std::string, double> w{"a", 45.0};

  // alternatively, using make_tuple(...) method
  std::tuple<std::string, double> x  = std::make_tuple("a", 45.0);
  //
  // End of Pre C++17 block
  //

  return 0;
}
```
## Works In Cases of Partial Class Template Specialization

```c++
#include <iostream>

template<typename T, typename U>
struct MyClass
{
    public:
    MyClass(T x, U y): _x(x), _y(y)
    {
        std::cout << "Primary Template" << std::endl;
    }

    private:
    T _x;
    U _y;
};

template<typename T>
struct MyClass<T,T>
{
    public:
    MyClass(T x, T y): _x(x), _y(y)
    {
        std::cout << "Partial Specialization 1" << std::endl;
    }

    private:
    T _x;
    T _y;
};

template<>
struct MyClass<int,int>
{
    public:
    MyClass(int x, int y): _x(x), _y(y)
    {
        std::cout << "Partial Specialization 2" << std::endl;
    }

    private:
    int _x;
    int _y;
};



int main()
{
    MyClass a{5,6};
    // compiler generates:
    // call    MyClass<int, int>::MyClass(int, int)
    //

    MyClass b{'c',6};
    // compiler generates:
    // MyClass<char, int>::MyClass(char, int)

    return 0;
}
```
Compiler returns:
```bash
Program returned: 0
Partial Specialization 2
Primary Template
```

## Case Where The Compiler Is Not Able To Infer Types

1. Instantation of `std::unique_ptr`:
```c++
#include <memory>
int main()
{
    // C++17 still requires explicit specification of the type.
    //               |
    //             ┌---┐
    std::unique_ptr<int>    p1(new int);


    // Error: C++17 cannot automatically deduce type from arguments.
    //                 |
    //               ┌---┐
    std::unique_ptr         p2(new int);
}
```

2. Instantation of `std::shared_ptr`.
