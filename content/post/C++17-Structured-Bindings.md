+++
author = "Alpine Swift"
title = "C++17: Structured Bindings, or, _Look, Python!_"
date = "2021-04-04"
description = "What the magic is?"
tags = [
    "C++17",
    "Programming",
]
+++

C++17 has beginning to make more sense, syntax-wise. One improvement in the 2017 standard `C++-17` is the concept of _structured bindings._
Here is a quick cheat-sheet that explains structured bindings.
<!--more-->
---
## Quick Theory
"Binds the specified names to subobjects or elements of the initializer. " by [cppreference.com](https://en.cppreference.com/w/cpp/language/structured_binding). Here's a picture to explain:

![Overview of structured bindings.](/notes-by-computer-engineer/post/pictures-c++17-structured-bindings/concept.png)
<!-- See overview [here](/notes-by-computer-engineer/post/pictures-c++17-structured-bindings/concept.png) -->

The ```ReturnType``` here is an example of a type that holds a structure. The concept works equally well with an ```std::tuple```, an ```std::pair```, and so on.


## Structure Data From Method or Function Without Custom Type
Passing data (e.g., `struct`, or _plain-old-data_) between functions (or methods) does not require explicitly declaring the `struct`. This simplifies things quite a bit:

```c++
#include <tuple>
#include <iostream>
//
//                          previously, you would have
//                          to declare a type here.
//                                   |
//                                   |
//                      ┌ ---------------------------┐    
auto sender(int num) -> std::tuple<int, char, double> {
    int a = num;
    char b = 'z';
    double c = num*10.0;
    return {a,b,c};
}

auto main() -> int
{
  // previously, you would have to instantiate an
  // object with a type to hold the return values.
  //       |
  //┌ ---------------┐
    auto [a_, b_, c_] = sender(5);
    std::cout << "a_: " << a_ << std::endl;
    std::cout << "b_: " << b_ << std::endl;
    std::cout << "c_: " << c_ << std::endl;
    return 0;
}

```

The compiler returns:
```bash
Program returned: 0
a_: 5
b_: z
c_: 50
```
## Bind to References
It is possible to bind to references as well, should it be necessary:

```c++
#include <tuple>
#include <iostream>

auto sender_by_reference(std::tuple<int, char, double>& ref_) -> void {
    // print address of ref_
    std::cout << "Address inside sender_by_reference: " << &(ref_) << std::endl;

    ref_ = {89, 'c', 99.38};

}

auto main() -> int
{

    std::tuple<int, char, double> data{9, 'c', 99.8};
    sender_by_reference(data);

    //     a_ref, b_ref, c_ref are references to members of the tuple.
    //              |
    //              |
    //    ┌ ------------------┐
    auto& [a_ref, b_ref, c_ref] = data;

    // print address of a_ref
    std::cout << "Address inside main(): " << &(data) << std::endl;

    std::cout << "a_ref: " << a_ref << std::endl;
    std::cout << "b_ref: " << b_ref << std::endl;
    std::cout << "c_ref: " << c_ref << std::endl;

    return 0;
}
```

The address of `ref_` inside the function `sender_by_reference` matches the address of `ref_` inside ```main()```:

```bash
Program returned: 0
Address inside sender_by_reference: 0x7ffd6ef8b380
Address inside main():              0x7ffd6ef8b380
a_ref: 89
b_ref: c
c_ref: 99.38
```

### More Conventional Example
More conventionally, the structure of `std::tuple<int, char, double>` can be organized inside a user-type, e.g., `ReturnType` below:
```c++
#include <tuple>
#include <iostream>

//     a custom type to hold the structure, instead of std::tuple
//         |
//    ┌ ---------┐
struct ReturnType
{
    int a;
    char b;
    double c;
};
//                       ref is now of explicitly defined type above.
//                           |
//                       ┌--------┐
auto sender_by_reference(ReturnType& ref_) -> void {
    // print address of ref_
    std::cout << "Address: " << &(ref_) << std::endl;

    ref_ = {44, 'x', 45.0};

}

auto main() -> int
{
  // instantiate an object of explicit type
  //    |
  //┌--------┐
    ReturnType data;

    //                send in data as reference
    //                    |   
    //                  ┌--┐
    sender_by_reference(data);
    auto& [a_ref, b_ref, c_ref] = data;

    // print address of a_ref
    std::cout << "Address: " << &(data) << std::endl;

    std::cout << "a_ref: " << a_ref << std::endl;
    std::cout << "b_ref: " << b_ref << std::endl;
    std::cout << "c_ref: " << c_ref << std::endl;

    return 0;
}
```
## Structured Bindings To Prevent Scope Leaks
It is now possible to tighten the lifetime of symbols to within an `if` condition or a `for` loop.

### The `if` condition:
```c++
#include <iostream>

auto myFunction(const int a, const int b) -> std::pair<int, int >
{
    // create two ints with random values to be returned
    return(std::pair<int, int>{a*2, a+b*3});

}

auto main() -> int
{
    //   return values are captured inside the if(...) block
    //   c, d are not visible outside of the if(...) block
    //   thereby preventing any scope leaks
    //          |
    //        ┌--┐
    if (auto [c, d] = myFunction(1,3); c != d)
    //  └---------------------------┘
    //               |
    //  notice that c and d can be initialized inside the
    //  if block itself (another C++17 feature)
    {
        std::cout << "something happened here." << std::endl;
    }

    return 0;
}
```
Everything runs as expected:
```bash
Program returned: 0
something happened here.
```
### The `for` loop:
```c++
#include <iostream>

auto myFunction(const int a) -> std::pair<int, int >
{
    // create two ints with random values to be returned
    return(std::pair<int, int>{a*1, a*3});

}

auto main() -> int
{
  //   return values are captured inside the for(...) block
  //   c, d are not visible outside of the for(...) block
  //   thereby preventing any scope leaks
  //             |
  //           ┌--┐
    for (auto [c, d] = myFunction(3); c < d; c++)
    {
        std::cout << "looping..." << std::endl;
    }

    return 0;
}
```

We get:
```bash
Program returned: 0
looping...
looping...
looping...
looping...
looping...
looping...
```
## Bonus Example: Key-Value Pairs
Iterating through key-value pairs (e.g., `std::map` type which provides iterators over keys and values) is now as easy as Python:
```c++
#include <map>
#include <iostream>
auto main() -> int
{

    std::map< int, char > mymap;
    mymap.insert ( { 1, 'y' } );
    mymap.insert ( { 2, 'z' } );

    // mymap will bind the key-part and value-part of the map to the
    // loop variables key, value. Like in Python. Or even Perl.
    // the datatype (here, std::map) must provide iterators over all fields
    // in order to binding to be successful.
    //                       |
    //                 ┌----------┐
    for ( const auto& [ key, value ] : mymap ) {
        if ( key == 1 ) { std::cout <<"Key:1, Value: " << value << std::endl; }
        if ( key == 2 ) { std::cout <<"Key:2, Value: " << value << std::endl; }
    }

}
```
