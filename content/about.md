+++
title = "About"
description = "lol"
date = "2021-03-29"
aliases = ["about-us", "about-hugo", "contact"]
author = "Alpine Swift"
+++

This place will have a collection of notes by a computer engineer in the hope
that these help others who are lost in academics or who need to master that concept _now_
in order to solve an issue at work _today_ or else the Earth will stand still.
