\input{PreRequisitesBayesTheorem}

\subsubsection*{Bayes' Theorem Part 1: Rare Events Require Increasingly Accurate Evidence}
\begin{table}[!h]
	\begin{tabular}{>{\raggedleft}p{0.25\textwidth}  p{0.75\textwidth}}
		\textbf{\textit{What Does It Answer?}}
		&
		\vspace*{-3em}
		\begin{center}

			\includegraphics[width=0.75\linewidth]{pictures/WhatIsBayesTheoremNutshell}
			%\caption[Cost of Owning a Tesla Model 3 Over the Years.]{}
			\label{fig:WhatIsBayesTheoremNutshell}
		\end{center}
		\FloatBarrier

		Combines the results of an imperfect observation (e.g., a clinical test for a disease which does not have 100\% accuracy) together with existing \textit{independently acquired} knowledge about the prevalence of that event (e.g., statistically, a small percentage of population actually has the disease) into an overall \textit{chance} of whether a person may actually have the  disease.

		\tabularnewline
		\noalign{\vskip0.02\textheight}

		\textbf{How To Remember Usage}
		&
		Whenever you are told that an event has occurred (e.g., sickness) based on an observation (e.g., clinical test result), but you end up doubting the event, and ask \textit{is it really, though?}
	\end{tabular}
\end{table}



Taking an example described above, we would like to find out the chance that a person has contracted an infection. We have the following information with us:

\begin{enumerate}
\item The medical test is known to be 90\% accurate.
\item It is known that about 40\% of the population where the person lives has the infection
\end{enumerate}

The general idea is shown in Figure \ref{fig:IntuitionBehindBayesTheorem} that follows.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.75\linewidth]{pictures/IntuitionBehindBayesTheorem}
	\caption[]{The Intuition behind the mathematics of Bayes' Theorem.}
	\label{fig:IntuitionBehindBayesTheorem}
\end{figure}
\FloatBarrier

\paragraph{Example: Covid-19 Positive Tests}
The city of London, England \href{https://data.london.gov.uk/dataset/coronavirus--covid-19--cases}{records the number} of Covid-19 infections which are available in a \href{https://data.london.gov.uk/download/coronavirus--covid-19--cases/d15e692d-5e58-4b6e-80f2-78df6f8b148b/phe_cases_age_london.csv}{spreadsheet}. Here is the relevant summary:
\begin{table}[!h]
	\begin{tabular}{>{\raggedleft}p{0.25\textwidth}  p{0.75\textwidth}}
		\textbf{Time Range}
		&
		28 Februrary 2021 - 6 March 2021
		\tabularnewline
		\noalign{\vskip0.02\textheight}

		\textbf{Estimated Population of London}
		&
		~9 Million
		\tabularnewline
		\noalign{\vskip0.02\textheight}

		\textbf{Total Confirmed Infections}
		&
		3699
		\tabularnewline
		\noalign{\vskip0.02\textheight}

		\textbf{Prevalence Rate}
		&
		$\frac{3699}{9 \text{M}} = 0.041\%$
		\tabularnewline
		\noalign{\vskip0.02\textheight}

		\textbf{Accuracy of Test}
		&
		\href{https://myhealthcareclinic.com/medical/covid-19-testing/}{98\%}
		\begin{flushleft}
		\includegraphics[width=0.6\linewidth]{pictures/AccuracyOfCovidTest}
		\label{fig:AccuracyOfCovidTest}
		\end{flushleft}
	\end{tabular}
\end{table}

Let's assume that Sarah, who lives in London, England has just tested positive for Covid-19. We would like to know the chances that Sarah is indeed infected, given the information we have already.

%\todo[linecolor=blue!20!white, backgroundcolor=blue!20!white,bordercolor=blue!20!white, inline]{Introduce the concept of True Positive and False Positive before Bayes' Theorem. }

In order to fill the data required for Figure \ref{fig:IntuitionBehindBayesTheorem}, compute $ P(\text{positive-test}) $ as:

\begin{align*}
P(\text{positive-test}) =& P(\text{positive-test}|\text{infected})\cdot P(\text{infected}) \tag*{\textcolor{gray}{\textit{// true positive}}} \\
+& P(\text{positive-test}|\text{not-infected})\cdot P(\text{not-infected}) \tag*{\textcolor{gray}{\textit{// false positive}}}\\
& \\
&= (0.98 \cdot 0.00041) + (0.02 \cdot 0.99959) \\
&= 0.0004018 + 0.0199918 \\
&= 0.0204
\end{align*}

Fill the data in Figure \ref{fig:IntuitionBehindBayesTheorem} to get:
\begin{align*}
P(\text{\text{infected}\,|\,positive-test}) &= \frac{(0.98) \cdot (0.00041)}{0.0204} \\
&= 0.0197\,\,\,\text{or, 1.97\% chance}
\end{align*}

\paragraph{What Did The Math Tell Us?} From the expressions above, we see that the chance that the 98\% accurate positive medical incorrectly flags a healthy individual (false positive rate) is 49.75 times greater (i.e., 0.0004018 vs 0.0199918) than the chance that the medical test correctly flags an infected person (true positive rate). Specifically, in this case, given that the disease is \textit{rare}, i.e. prevalence in our example is $ 0.041\% $, the Covid-19 test is not accurate \textit{enough}. That is, the \textit{less prevalent} the disease, \textit{the more} accuracy required of the test in order to have any reasonable confidence in the test.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\linewidth]{pictures/Exampe_Bayes_1}
	\caption[]{A simulation showing that in order to have a significant chance that an observation correctly indicates an event, e.g., $ P(\text{infected}|\text{positive-test}) > 0.6$, the accuracy required of the observation (e.g., test result) increases significantly as the event becomes increasing unlikely (e.g., very low prevalence of infection in a population)}.
	\label{fig:Exampe_Bayes_1}
\end{figure}
\FloatBarrier

\paragraph{We Act Bayes' Theorem All The Time. } Consider a scenario when a young child runs into you and tells you that there are real fairies outside distributing ice-cream to everyone, while you lay inside on your couch facing the television (or a wall). Would you believe the child's words (i.e., the evidence, or in the case of disease, a medical test report) instantly? Quite unlikely. This child may be misinterpreting something totally mundane with fairies (i.e., the chance that the test diagnosis is faulty). You will likely have believed the same words if these had come from a figure of authority such as your spouse or parents (i.e., the test results are much more accurate). This natural act of suspicion of a rare event (the fairies and related ice-cream) unless the observation (the messenger: child or figure of authority) has the  precision \textit{adequate} \textit{for the rarity of the event} is mathematically captured in Bayes' Theorem.