#!/usr/bin/env bash
find ./ -maxdepth 1 -type f ! -name '*.tex' -a ! -name '*.sh' -exec rm -rf {} \;
