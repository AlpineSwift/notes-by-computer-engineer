## Copyright (C) 2021 Stingray
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} simulate (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Stingray <stingray@stingray>
## Created: 2021-03-14

function retval = simulate (prevalence, accuracyOfTest)
p = [0:0.1:prevalence];
a = [0:0.1:accuracyOfTest];
c = (a'*p)./((a'*p) + (1-a)'*(1-p));

graphics_toolkit qt
surfl (p, a, c);
xlabel("Prevalence");
ylabel("Accuracy");
zlabel("Actual Chance");
print("here");
endfunction
