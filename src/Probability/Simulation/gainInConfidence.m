## Copyright (C) 2021 Stingray
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} gainInConfidence (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Stingray <stingray@stingray>
## Created: 2021-03-17

function retval = gainInConfidence ()
  confidence = [0.32, 0.81, 0.97];
  nObservations = [1, 2, 3];
  #breaks = [1,2,3];
  #pp1 = splinefit (nObservations, confidence, breaks);
  #y1 = ppval (pp1, [1,2,3]);
  plot(nObservations, confidence, '-bo'); hold on;
  #plot([1,2,3], y1, '-kx'); hold off;
  xlabel("Number of Observation");
  ylabel("Chance of Pedestrian Stepping Onto Crosswalk");
endfunction
